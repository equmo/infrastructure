/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 02.01.14 23:05

package org.equmo.docs.generate.pdf;

import com.google.template.soy.data.SoyData;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.equmo.docs.generate.soy.SoyCompilerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;

@ContextConfiguration({
        "classpath*:META-INF/spring/*.xml",
        "classpath:META-INF/test-spring/*.xml"
})
public class PdfGeneratorServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private Resource testHtml;

    @Autowired
    private Resource testCss;

    @Autowired
    private Resource testImage;

    @Autowired
    private String tmpPath;

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @Autowired
    private SoyCompilerService soyCompilerService;

    @Autowired
    private Resource parentTestTemplate;

    @Autowired
    private Resource documentLayoutTemplate;

    @Autowired
    private Resource childTestTemplate;

    @Autowired
    private String mainLayoutTemplateName;

    private List<Resource> testTemplates = new ArrayList<>();

    Map<String, byte[]> resourceMap = new HashMap<>();

    private byte[] result;

    @Autowired
    private Boolean writeResultToFile;

    @BeforeMethod
    public void setUp() throws Exception {
        InputStream iCssStream = new FileInputStream(testCss.getFile());
        byte[] bCss = IOUtils.toByteArray(iCssStream);
        iCssStream.close();

        InputStream iImageStream = new FileInputStream(testImage.getFile());
        byte[] bImage = IOUtils.toByteArray(iImageStream);
        iImageStream.close();

        resourceMap.clear();
        resourceMap.put("test.css", bCss);
        resourceMap.put("test_image.png", bImage);

        testTemplates.clear();
        testTemplates.add(documentLayoutTemplate);
        testTemplates.add(parentTestTemplate);
        testTemplates.add(childTestTemplate);
    }

    @AfterMethod
    public void afterMethod() throws Exception {

        if (writeResultToFile) {
            FileUtils.forceMkdir(new File(tmpPath));
            OutputStream os = new FileOutputStream(tmpPath + File.separator + PdfGeneratorServiceTest.class.getSimpleName() + RandomStringUtils.randomAlphabetic(5) + ".pdf");
            os.write(result);
            IOUtils.closeQuietly(os);
            result = null;
        }
    }

    @Test
    public void generateSimplePdfTest() throws Exception {
        InputStream iHtmlStream = new FileInputStream(testHtml.getFile());
        String sHtml = IOUtils.toString(iHtmlStream);
        iHtmlStream.close();

        result = pdfGeneratorService.generatePdfFromHtml(sHtml, resourceMap);
        assertThat(result).isNotEmpty();
    }

    @Test
    public void generatePdfFromSoyTest() throws Exception {
        Map<String, Object> variables = new HashMap<>();
        variables.put("subTemplateName", SoyData.createFromExistingData("testTemplate"));
        variables.put("testVariable", SoyData.createFromExistingData("Some Test Value"));
        variables.put("sectionsHtml", SoyData.createFromExistingData("Some Test Value"));
        String compiledSoy = soyCompilerService.compileSoyToHtml(this.testTemplates, mainLayoutTemplateName, variables);

        result = pdfGeneratorService.generatePdfFromHtml(compiledSoy, resourceMap);
        assertThat(result).isNotEmpty();
    }
}
