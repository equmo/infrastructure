/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.runner;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.logging.LogManager;


/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class ServerRunner {

    private static final Logger log = LoggerFactory.getLogger(ServerRunner.class);

    public static void main(String[] args) throws IOException {
        SLF4JBridgeHandler.install();
        LogManager.getLogManager().readConfiguration(IOUtils.toInputStream("handlers = org.slf4j.bridge.SLF4JBridgeHandler"));

        BuildNumber.logImplementationVersions();

        ConfigurableApplicationContext context = new GenericXmlApplicationContext(new ClassPathResource("META-INF/spring/equmo-server-runner.xml"));
        EmbeddedServer embeddedServer = context.getBean(EmbeddedServer.class);
        embeddedServer.start();
    }

}
