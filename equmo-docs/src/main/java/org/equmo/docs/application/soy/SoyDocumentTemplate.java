/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application.soy;

import org.springframework.core.io.Resource;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 20.01.14
 */
public class SoyDocumentTemplate {

    private final Resource layoutSoyResource;

    private final String templateName;

    private final Map<String, Resource> resources;

    public SoyDocumentTemplate(Resource layoutSoyResource, String templateName, Map<String, Resource> resources) {
        this.layoutSoyResource = layoutSoyResource;
        this.templateName = templateName;
        this.resources = resources;
    }

    public Resource getLayoutSoyResource() {
        return layoutSoyResource;
    }

    public String getTemplateName() {
        return templateName;
    }

    public Map<String, Resource> getResources() {
        return resources;
    }

}
