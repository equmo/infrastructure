/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.email.application;

import org.apache.commons.lang3.Validate;
import org.equmo.docs.generate.soy.SoyCompilerService;
import org.equmo.email.infrastructure.EmailGateway;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by kepkap on 09/10/14.
 */
public class EmailServiceImpl implements EmailService {


    @Autowired
    private EmailGateway emailGateway;

    @Autowired
    private SoyCompilerService soyCompilerService;


    private String fromEmail;


    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public void send(String toEmail, String subject, String body) {
        Validate.notEmpty(fromEmail);
        emailGateway.send(fromEmail, toEmail, subject, body);
    }

    public void send(String toEmail, String subject, EmailTemplate emailTemplate, Map<String, Object> contextVariables) {
        String payload = soyCompilerService.compileSoyToHtml(
                emailTemplate.getTemplateResources(),
                emailTemplate.getTemplateName(),
                emailTemplate.getVariables(contextVariables));

        send(toEmail, subject, payload);
    }
}
