/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 01.02.14
 */
public class ContextData {

    private final Map<String, Object> data;

    public ContextData() {
        data = new HashMap<>();
    }

    public void put(String key, Object value) {
        data.put(key, value);
    }

    public Object get(String key) {
        Object value = data.get(key);
        if (value == null) {
            throw new IllegalArgumentException("data has no entry with key=" + key);
        }
        return value;
    }

    public static ContextData from(String key, Object value) {
        ContextData contextData = new ContextData();
        contextData.put(key, value);
        return contextData;
    }
}
