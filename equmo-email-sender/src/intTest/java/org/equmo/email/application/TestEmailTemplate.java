package org.equmo.email.application;

import org.springframework.core.io.Resource;

import java.util.List;

public class TestEmailTemplate extends EmailTemplate {

    private static final String SUB_TEMPLATE_NAME = "testTemplate";

    public TestEmailTemplate(String subject, String templateName, List<Resource> templateFileNames) {
        super(subject, templateName, templateFileNames);
        addTemplateVariable("subTemplateName", SUB_TEMPLATE_NAME);
    }

}
