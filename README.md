# Infrastructure  modules #

*If you have no idea where to put nodule, - put it here, that will do *

## Server Runner (equmo-server-runner) ##
Is actually an entry point, main class with Jetty Web Server and Spring context scanning path configuration. It's main class org.equmo.WebserverRunner is called from Installer's repository service wrapper.

## Utility classes for REST (equmo-rest) ##
A common REST API helpers.

## Email sending (equmo-email-sender, equmo-email) ##
APIs for sending emails. works using spring integration email channel.

## PDF exporter (equmo-docs) ##
Module to work with PDF documents. Abstract framework for generating PDFs.


