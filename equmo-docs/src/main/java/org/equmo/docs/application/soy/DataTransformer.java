/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application.soy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 24.01.14
 */
public abstract class DataTransformer {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormat.forStyle("LS").withLocale(new Locale("ru", "RU"));

    public static Map<String, Object> transform(Map<String, Object> variables) {
        Map<String, Object> result = new HashMap<>(variables.size());
        for (Map.Entry<String, Object> entry : variables.entrySet()) {
            result.put(entry.getKey(), transform(entry.getValue()));
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private static Object transform(Object object) {
        if (object instanceof String) {
            return object;
        } else if (object instanceof Boolean) {
            return object.toString();
        } else if (object instanceof Number) {
            return object.toString();
        } else if (object instanceof List) {
            List<Object> transformed = new ArrayList<>();
            for (Object o : (List) object) {
                transformed.add(transform(o));
            }
            return transformed;
        } else {
            ObjectMapper m = new ObjectMapper();
            SimpleModule module = new SimpleModule();
            module.addSerializer(Long.class, new ToStringSerializer());
            module.addSerializer(Date.class, new CustomJsonDateSerializer());
            m.registerModule(module);
            return m.convertValue(object, Map.class);
        }
    }
}
