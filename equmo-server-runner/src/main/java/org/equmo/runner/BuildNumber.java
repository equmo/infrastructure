/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.runner;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * Created by kepkap on 27/10/14.
 */
public class BuildNumber {

    private static final Logger log = LoggerFactory.getLogger(BuildNumber.class);

    public static void logImplementationVersions() {

        List<Pair<String, String>> pairs = new ArrayList<>();
        try {
            Enumeration<URL> resources = BuildNumber.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
            while (resources.hasMoreElements()) {
                Manifest manifest = new Manifest(resources.nextElement().openStream());
                Attributes attributes = manifest.getMainAttributes();
                String moduleGroup = attributes.getValue("Module-Group");
                String moduleName = attributes.getValue("Module-Name");
                String buildNumber = attributes.getValue("Build-Number");
                String version = attributes.getValue(Attributes.Name.IMPLEMENTATION_VERSION);
                if (moduleName != null) {
                    pairs.add(Pair.of(String.format("%s - %s : %s", version, moduleGroup, moduleName), StringUtils.abbreviate(buildNumber, 16)));
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Collections.sort(pairs, new Comparator<Pair<String, String>>() {
            @Override
            public int compare(Pair<String, String> o1, Pair<String, String> o2) {
                return o1.getLeft().compareTo(o2.getLeft());
            }
        });

        for (Pair<String, String> pair : pairs) {
            log.info("{}, {}", pair.getLeft(), pair.getRight());
        }
    }
}
