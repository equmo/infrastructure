/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application;

import org.equmo.docs.application.soy.SoyDocumentTemplate;
import org.equmo.docs.application.soy.bookmarks.Bookmarks;
import org.springframework.beans.factory.BeanNameAware;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 05.02.14
 */
public class TestSoyDocument implements SoyDocument, BeanNameAware {


    private final SoyDocumentTemplate template;
    private final List<SoyPart> parts;
    private final List<SoySection> sections;
    private final Bookmarks bookmarks;
    private String name;

    public TestSoyDocument(SoyDocumentTemplate template, List<SoyPart> parts, List<SoySection> sections, Bookmarks bookmarks) {
        this.template = template;
        this.parts = parts;
        this.sections = sections;
        this.bookmarks = bookmarks;
    }

    @Override
    public Bookmarks getBookmarks() {
        return bookmarks;
    }

    @Override
    public SoyDocumentTemplate getTemplate() {
        return template;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<SoyPart> getParts() {
        return parts;
    }

    @Override
    public List<SoySection> getSections() {
        return sections;
    }

    @Override
    public Map<String, byte[]> getResources() {
        return Collections.emptyMap();
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Override
    public Map<String, Object> getData(ContextData contextData) {
        return Collections.emptyMap();
    }
}
