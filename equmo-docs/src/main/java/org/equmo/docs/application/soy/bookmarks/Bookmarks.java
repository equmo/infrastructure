/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application.soy.bookmarks;

import org.equmo.docs.application.ContextData;
import org.equmo.docs.application.SoyPart;
import org.equmo.docs.application.SoySection;
import org.equmo.docs.application.soy.SoyPartTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bookmarks implements SoyPart {


    private final SoyPartTemplate template;
    private final List<SoySection> sections;
    private final String name;

    public Bookmarks(List<SoySection> sections, SoyPartTemplate template) {
        this.template = template;
        this.sections = sections;
        this.name = "bookmarks";
    }

    @Override
    public SoyPartTemplate getTemplate() {
        return template;
    }

    @Override
    public Map<String, byte[]> getResources(ContextData contextData) {
        return Collections.emptyMap();
    }

    @Override
    public Map<String, Object> getData(ContextData contextData) {
        List<Bookmark> bookmarks = new ArrayList<>(sections.size());
        for (SoySection section : sections) {
            bookmarks.add(new Bookmark(section.getName(), section.getTitle()));
        }

        Map<String, Object> result = new HashMap<>(1);
        result.put("sections", bookmarks);
        return result;
    }

    @Override
    public String getName() {
        return name;
    }
}
