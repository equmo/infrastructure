/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.datasource.util.schema;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.Set;

/**
 * @author Alexander Fyodorov
 * @author Denis Kuchugurov
 */
public class SchemaGenerator {

    private static Logger logger = LoggerFactory.getLogger(SchemaGenerator.class);

    public static void main(String[] args) throws Exception {
        String rootPackage = args[0];
        String outputFile = args[1];

        Configuration configuration = new Configuration();
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");

        Set<Class> persistentClasses = new JPAClassesScanner(rootPackage, new DefaultResourceLoader()).scan();


        logger.info("total classes scanned for ddl generation: ", persistentClasses.size());
        for (Class clazz : persistentClasses) {
            logger.info("class scanned for ddl generation: {}", clazz.getName());
            configuration.addAnnotatedClass(clazz);
        }

        SchemaExport schema = new SchemaExport(configuration);
        schema.setOutputFile(outputFile);
        schema.setDelimiter(";");
        schema.execute(false, false, false, true);
    }

}
