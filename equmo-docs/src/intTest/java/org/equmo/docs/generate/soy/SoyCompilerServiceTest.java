/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 02.01.14 17:03

package org.equmo.docs.generate.soy;

import com.google.template.soy.data.SoyData;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;


@ContextConfiguration({
        "classpath*:META-INF/spring/*.xml",
        "classpath:META-INF/test-spring/*.xml"
})
public class SoyCompilerServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SoyCompilerService soyCompilerService;

    @Autowired
    private String templateName;

    @Autowired
    private String tmpPath;

    @Autowired
    private Resource parentTestTemplate;

    @Autowired
    private Resource childTestTemplate;

    private List<Resource> testTemplates = new ArrayList<>();

    private byte[] result;

    @Autowired
    private Boolean writeResultToFile;

    @BeforeMethod
    public void setUp() throws Exception {
        testTemplates.clear();
        testTemplates.add(parentTestTemplate);
        testTemplates.add(childTestTemplate);
    }

    @AfterMethod
    public void afterMethod() throws Exception {

        if (writeResultToFile) {
            FileUtils.forceMkdir(new File(tmpPath));
            OutputStream os = new FileOutputStream(tmpPath + File.separator + SoyCompilerServiceTest.class.getSimpleName() + RandomStringUtils.randomAlphabetic(5) + ".html");
            os.write(result);
            IOUtils.closeQuietly(os);
            result = null;
        }
    }


    @Test
    public void compileSimpleTemplateTest() throws Exception {
        Map<String, Object> variables = new HashMap<>();
        variables.put("subTemplateName", SoyData.createFromExistingData("testTemplate"));
        variables.put("testVariable", SoyData.createFromExistingData("Some Test Value"));

        String html = soyCompilerService.compileSoyToHtml(this.testTemplates, templateName, variables);
        result = html.getBytes();
        assertThat(result).isNotEmpty();
    }
}
