/*
 * Copyright (c) 2014.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.datasource.util.base64;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;

/**
 * Created by Denis Kuchugurov on 20.04.2014.
 * <p/>
 * Here is usage pattern, used for generating base64 Ids for jpa entities.
 * <p/>
 * -@Id
 * -@GeneratedValue(generator = "base64")
 * -@GenericGenerator(name = "base64", strategy = "org.equmo.pmbok.core.domain.model.Base64IdGenerator")
 * -private String id;
 */
@SuppressWarnings("unused")
public class Base64IdGenerator implements IdentifierGenerator {

    @Override
    public Serializable generate(SessionImplementor session, Object object) throws HibernateException {
        return Base64KeyEncoder.random();
    }

}
