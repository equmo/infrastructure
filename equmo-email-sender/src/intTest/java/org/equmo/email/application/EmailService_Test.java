/*
 * Copyright 2012 - 2013 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.email.application;

import com.jayway.awaitility.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.concurrent.Callable;

import static com.jayway.awaitility.Awaitility.await;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 30.10.13
 */
@ContextConfiguration({
        "classpath*:META-INF/spring/*.xml",
        "classpath:META-INF/test-spring/test-equmo-email.xml"
})
public class EmailService_Test extends AbstractTestNGSpringContextTests {


    @Autowired
    private TestEmailTemplate template;

    @Autowired
    private EmailService emailService;


    @Test(enabled = false)
    public void test_send_template() throws Exception {
        emailService.send("kuchugurov@gmail.com", template.getSubject(), template, Collections.<String, Object>emptyMap());

        await().atMost(Duration.TEN_SECONDS).until(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return false;
            }
        });
    }

    @Test(enabled = false)
    public void test_send() throws Exception {
        emailService.send("kuchugurov@gmail.com", "subject-asd", "test email");

        await().atMost(Duration.TEN_SECONDS).until(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return false;
            }
        });
    }
}
