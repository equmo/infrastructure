/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 03.01.14 23:11

package org.equmo.docs.generate.sass;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static org.fest.assertions.api.Assertions.assertThat;

@ContextConfiguration({
        "classpath*:META-INF/spring/*.xml",
        "classpath:META-INF/test-spring/*.xml"
})
public class SassCompilerServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private SassCompilerService sassCompilerService;

    @Autowired
    private Resource testScss;

    @Autowired
    private String tmpPath;
    private byte[] result;

    @Autowired
    private Boolean writeResultToFile;


    @AfterMethod
    public void afterMethod() throws Exception {

        if (writeResultToFile) {
            FileUtils.forceMkdir(new File(tmpPath));
            OutputStream os = new FileOutputStream(tmpPath + File.separator + SassCompilerServiceTest.class.getSimpleName() + RandomStringUtils.randomAlphabetic(5) + ".css");
            os.write(result);
            IOUtils.closeQuietly(os);
            result = null;
        }
    }

    @Test
    public void compileSimpleScssTest() throws Exception {
        result = sassCompilerService.compileSass(testScss.getFile()).getBytes();
        assertThat(result).isNotEmpty();
    }
}
