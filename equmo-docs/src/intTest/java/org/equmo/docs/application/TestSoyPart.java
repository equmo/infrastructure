/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application;

import org.equmo.docs.application.soy.SoyPartTemplate;
import org.springframework.beans.factory.BeanNameAware;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 22.01.14
 */
public class TestSoyPart implements SoySection, BeanNameAware {

    private final SoyPartTemplate template;
    private final Map<String, Object> variables;
    private String name;

    public TestSoyPart(SoyPartTemplate template, Map<String, Object> variables) {
        this.template = template;
        this.variables = variables;
    }

    @Override
    public String getTitle() {
        return getName();
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public SoyPartTemplate getTemplate() {
        return template;
    }

    @Override
    public Map<String, byte[]> getResources(ContextData contextData) {
        return ResourceHelper.getContent(template.getResources());
    }

    @Override
    public Map<String, Object> getData(ContextData contextData) {
        variables.put("testLong", new Long(1l));
        variables.put("testData", new TestData(1l, "asd", new Date()));
        variables.put("testDataList", Arrays.asList(new TestData(1l, "asd", new Date())));
        return variables;
    }
}
