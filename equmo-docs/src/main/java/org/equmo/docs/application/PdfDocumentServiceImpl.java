/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.docs.application;

import org.apache.commons.lang3.Validate;
import org.equmo.docs.application.soy.DataTransformer;
import org.equmo.docs.application.soy.bookmarks.Bookmarks;
import org.equmo.docs.generate.pdf.PdfGeneratorService;
import org.equmo.docs.generate.soy.SoyCompilerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Denis Kuchugurov
 *         Date: 21.01.14
 */
@Service
public class PdfDocumentServiceImpl implements PdfDocumentService {

    @Autowired
    private PdfGeneratorService pdfGeneratorService;

    @Autowired
    private SoyCompilerService soyCompilerService;

    @Override
    public byte[] generatePdf(ContextData contextData, SoyDocument soyDocument) {

        Resource layoutSoyResource = soyDocument.getTemplate().getLayoutSoyResource();
        String templateName = soyDocument.getTemplate().getTemplateName();
        List<? extends SoySection> sections = soyDocument.getSections();

        Validate.notNull(layoutSoyResource, "layoutSoyResource is null in <" + soyDocument.getName() + "> document");
        Validate.notEmpty(templateName, "templateName is empty in <" + soyDocument.getName() + "> document");
        Validate.notEmpty(sections, "sections are empty in document <" + soyDocument.getName() + ">");


        StringBuilder sectionsContent = new StringBuilder();
        Map<String, byte[]> resources = new HashMap<>();
        resources.putAll(soyDocument.getResources());

        for (SoySection section : sections) {
            Map<String, Object> data = section.getData(contextData);

            sectionsContent.append(compileSoy(section, DataTransformer.transform(data)));
            resources.putAll(section.getResources(contextData));

        }

        StringBuilder partsContent = new StringBuilder();
        List<? extends SoyPart> parts = soyDocument.getParts();
        for (SoyPart part : parts) {
            partsContent.append(compileSoy(part, DataTransformer.transform(part.getData(contextData))));
            resources.putAll(part.getResources(contextData));
        }

        //add Table of Contents

        Map<String, Object> vars = new HashMap<>();
        vars.putAll(DataTransformer.transform(soyDocument.getData(contextData)));
        vars.put("partsHtml", partsContent.toString());
        vars.put("sectionsHtml", sectionsContent.toString());

        //add bookmarks
        Bookmarks bookmarks = soyDocument.getBookmarks();
        if (bookmarks != null) {
            vars.put("bookmarksHtml", compileSoy(bookmarks, DataTransformer.transform(bookmarks.getData(contextData))));
        }

        String result = soyCompilerService.compileSoyToHtml(Arrays.asList(layoutSoyResource), templateName, vars);

        return pdfGeneratorService.generatePdfFromHtml(result, resources);
    }

    private String compileSoy(SoyPart soyPart, Map<String, Object> variables) {
        List<Resource> soyResources = soyPart.getTemplate().getSoyResources();
        String sectionTemplateName = soyPart.getTemplate().getTemplateName();

        Validate.notNull(soyResources, "soyResources is null in <" + soyPart.getName() + "> section");
        Validate.notEmpty(soyResources, "soyResources is empty in <" + soyPart.getName() + "> section");
        Validate.notEmpty(sectionTemplateName, "templateName is empty in <" + soyPart.getName() + "> section");


        return soyCompilerService.compileSoyToHtml(soyResources, sectionTemplateName, variables);
    }
}
