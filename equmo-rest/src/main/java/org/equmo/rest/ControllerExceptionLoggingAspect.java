/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.rest;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Denis Kuchugurov on 12/10/14.
 */
@Aspect
@Component
public class ControllerExceptionLoggingAspect {


    private static Logger logger = LoggerFactory.getLogger(ControllerExceptionLoggingAspect.class);


    @AfterThrowing(pointcut = "@within(org.springframework.web.bind.annotation.RequestMapping)", throwing = "e")
    public void log(Exception e) {
        logger.debug("Controller has thrown exception", e);
    }
}
