/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 02.01.14 16:47

package org.equmo.docs.generate.soy;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.tofu.SoyTofu;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public class SoyCompilerServiceImpl implements SoyCompilerService {

    @Override
    public String compileSoyToHtml(List<Resource> templateResources, String templateName, Map<String, Object> variables) {
        SoyFileSet.Builder builder = new SoyFileSet.Builder();
        for (Resource templateResource : templateResources) {
            try {
                builder.add(templateResource.getURL());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        SoyTofu compiledTemplates = builder.build().compileToTofu();
        return compiledTemplates.newRenderer(templateName).setData(variables).render();
    }
}
