/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Created by Liventsev Andrey. 03.01.14 23:11

package org.equmo.docs.generate.sass;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;

@Service
public class SassCompilerServiceImpl implements SassCompilerService {

    @Override
    public String compileSass(File scssFile) throws IOException {
        String content = FileUtils.readFileToString(scssFile);
        if (StringUtils.isEmpty(content)) {
            throw new IllegalArgumentException("Sass content is empty");
        }

        try {
            ScriptEngine rubyEngine = new ScriptEngineManager().getEngineByName("jruby");
            return rubyEngine.eval(buildRubyScript(content, scssFile.getParentFile().getAbsolutePath())).toString();
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
    }

    private String buildRubyScript(final String content, String inputDir) {
        final StringWriter raw = new StringWriter();
        final PrintWriter script = new PrintWriter(raw);
        final StringBuilder options = new StringBuilder();
        options.append(":syntax => :scss, ");
        options.append(":load_paths => ['").append(inputDir.replace("\\", "/")).append("'] ");
        script.println(" require 'rubygems' ");
        script.println(" require 'sass/plugin' ");
        script.println(" require 'sass/engine' ");
        script.println(" source = '" + content.replace("'", "\"") + "' ");
        script.println(" Sass::Plugin.options[:style] = :compact ");
        script.println(" engine = Sass::Engine.new(source, {" + options.toString() + "}) ");
        script.println(" result = engine.render ");
        script.flush();
        return raw.toString();
    }

}
