package org.equmo.email.application;

import org.apache.commons.lang3.Validate;
import org.springframework.core.io.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class EmailTemplate {

    private final String subject;

    private final String templateName;

    private final List<Resource> templateResources;

    private final Map<String, Object> templateVariables;


    protected EmailTemplate(String subject, String templateName, List<Resource> templateFileNames) {
        this.subject = subject;
        this.templateName = templateName;
        this.templateResources = templateFileNames;
        this.templateVariables = new HashMap<>();
    }

    protected void addTemplateVariable(String name, Object value) {
        Validate.notEmpty(name);
        Validate.notNull(value);
        templateVariables.put(name, value);
    }

    public String getSubject() {
        return subject;
    }

    public Map<String, Object> getVariables(Map<String, Object> contextVariables) {
        Map<String, Object> all = new HashMap<>(templateVariables);
        all.putAll(contextVariables);
        return all;
    }


    public List<Resource> getTemplateResources() {
        return templateResources;
    }

    public String getTemplateName() {
        return templateName;
    }
}
