/*
 * Copyright 2014 www.equmo.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.equmo.runner;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.springframework.core.io.Resource;

import java.net.URL;

/**
 * Created by dkuchugurov on 21.05.2014.
 */
public class EmbeddedServer {

    private final ServletContextHandler servletContextHandler;

    private final String keyStorePassword;
    private final String keyManagerPassword;
    private final Resource keyStoreResource;
    private final Integer port;

    public EmbeddedServer(ServletContextHandler servletContextHandler, String keyStorePassword, String keyManagerPassword, Resource keyStoreResource, Integer port) {
        this.servletContextHandler = servletContextHandler;
        this.keyStorePassword = keyStorePassword;
        this.keyManagerPassword = keyManagerPassword;
        this.keyStoreResource = keyStoreResource;
        this.port = port;
    }

    public void start() {
        try {
            Server server = new Server();

            HttpConfiguration httpConfiguration = new HttpConfiguration();
            httpConfiguration.addCustomizer(new SecureRequestCustomizer());

            SslContextFactory sslContextFactory = new SslContextFactory();
            URL url = keyStoreResource.getURL();
            sslContextFactory.setKeyStorePath(url.getProtocol() + ":" + url.getPath());
            sslContextFactory.setKeyStorePassword(keyStorePassword);
            sslContextFactory.setKeyManagerPassword(keyManagerPassword);

            ServerConnector serverConnector = new ServerConnector(server, new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(httpConfiguration));

            serverConnector.setPort(port);
            server.setConnectors(new Connector[]{serverConnector});

            server.setHandler(servletContextHandler);

            server.start();
            server.setStopAtShutdown(true);
            server.join();
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(-1);
        }
    }
}
